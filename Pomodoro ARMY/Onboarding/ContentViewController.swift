//
//  ContentViewController.swift
//  Pomodoro ARMY
//
//  Created by Oleksandr Danylenko on 6/6/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import UIKit

class ContentViewController: UIViewController {
    
    @IBOutlet weak var pageLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    
    let pageContent = [
    (image: #imageLiteral(resourceName: "clipboard (1)"), text: "Создайте список\nдел"),
    (image: #imageLiteral(resourceName: "stopwatch"), text: "Запусите таймер"),
    (image: #imageLiteral(resourceName: "25"), text: "Работайте \n25 минут "),
    (image: #imageLiteral(resourceName: "break"), text: "Сделайте \nперерыв"),
    (image: #imageLiteral(resourceName: "plus"), text: "Продолжайте\nработу или ставьте \nновую задачу")
    ]
    
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pageLabel.text = "0" + String(index+1)
        textLabel.text = pageContent[index].text
        imageView.image = pageContent[index].image
        // Do any additional setup after loading the view.
        
        
        nextButton.isHidden = index != 4
        nextButton.tintColor = .white
        nextButton.layer.borderColor = UIColor.white.cgColor
        nextButton.layer.borderWidth = 1
        nextButton.layer.cornerRadius = (nextButton.frame.size.height / 2) - 2
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
