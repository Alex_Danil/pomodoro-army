//
//  OnboardingPageViewController.swift
//  Pomodoro ARMY
//
//  Created by Oleksandr Danylenko on 6/6/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import UIKit

class OnboardingPageViewController: UIPageViewController {

   var pages: [ContentViewController]!
    
    
    fileprivate func getViewController(withIdentifier identifier: String, index: Int) -> ContentViewController
    {
        let contentVC = UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: identifier) as! ContentViewController
        contentVC.index = index
        return contentVC
    }
    
    override func viewDidLoad()
    {
        
        pages = [self.getViewController(withIdentifier: "ContentVC", index: 0),self.getViewController(withIdentifier: "ContentVC", index: 1),self.getViewController(withIdentifier: "ContentVC", index: 2),self.getViewController(withIdentifier: "ContentVC", index: 3),self.getViewController(withIdentifier: "ContentVC", index: 4)]
        
        let backgroundImage = UIImageView(frame: self.view.frame)
        backgroundImage.image = #imageLiteral(resourceName: "bg")
        
        self.view.layer.insertSublayer(backgroundImage.layer, at: 0)
        
        
        
        
        super.viewDidLoad()
        self.dataSource = self
        self.delegate   = self
        
        
     
        
        if let firstVC = pages.first
        {
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
    }

}

extension OnboardingPageViewController : UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = pages.index(of: viewController as! ContentViewController) else { return nil }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0          else { return nil }
        
        guard pages.count > previousIndex else { return nil        }
        
        return pages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?
    {
        guard let viewControllerIndex = pages.index(of: viewController as! ContentViewController) else { return nil }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < pages.count else { return nil }
        
        guard pages.count > nextIndex else { return nil         }
        
        return pages[nextIndex]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 5
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    
}
