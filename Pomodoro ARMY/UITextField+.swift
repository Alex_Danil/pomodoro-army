//
//  UITextField+.swift
//  Pomodoro ARMY
//
//  Created by Oleksandr Danylenko on 6/10/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    
 
    
    func addButtonOnKeyboard(_ button: UIBarButtonItem )
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = button
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
 
}
