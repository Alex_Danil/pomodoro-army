//
//  TaskTableViewCell.swift
//  Pomodoro ARMY
//
//  Created by admin1 on 6/7/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class TaskTableViewCell: UITableViewCell {
	
    @IBOutlet weak var doneButton: UIButton! {
        
        didSet {
            doneButton.layer.borderColor = UIColor.lightGray.cgColor
        }
    }
    
	
	@IBOutlet weak var taskTextField: UITextField!
	
    var task : Task?
    
    private let disposeBag = DisposeBag()
    
	override func awakeFromNib() {
		super.awakeFromNib()
        
        taskTextField.rx.text
            .orEmpty
            .bind(onNext: { (text) in
                self.task?.text = text
            })
            .disposed(by: disposeBag)
        
		// Initialization code
	}
	
	override func setSelected(_ selected: Bool, animated: Bool) {
		super.setSelected(selected, animated: animated)
		
		// Configure the view for the selected state
	}
	
	func setupWithTask(_ task: Task) {
        self.task = task
		taskTextField.text = task.text
		doneButton.backgroundColor = !task.done ? .white : UIColor(named: "armColor")
	}
    
    func setupFillTask() {
        self.task = Task()
        taskTextField.text = ""
        doneButton.backgroundColor = .white
    }
	
    @IBAction func buttonTapped(_ sender: Any) {
        if let _task = self.task {
            self.task?.done = !_task.done
            self.doneButton.backgroundColor = !self.task!.done ? .white : UIColor(named: "armColor")
        }
    }
    
	
}
