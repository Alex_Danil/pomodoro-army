//
//  Task.swift
//  Pomodoro ARMY
//
//  Created by admin1 on 6/7/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object {
	
	@objc dynamic var done : Bool = false
	@objc dynamic var remind : Bool = false
	@objc dynamic var deadline : Date = Date(timeIntervalSince1970: 0)
	@objc dynamic var repeatDate : Int = 0
	@objc dynamic var text : String = ""
	
}
