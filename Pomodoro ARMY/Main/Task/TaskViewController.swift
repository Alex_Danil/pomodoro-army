//
//  TaskViewController.swift
//  Pomodoro ARMY
//
//  Created by admin1 on 6/7/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import UIKit

class TaskViewController: UIViewController {
	
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var toDoNameField: UITextField! {
        didSet {
            toDoNameField.font = UIFont.systemFont(ofSize: 32, weight: .medium)
        }
    }
    var tasks = [Task]()
    
    var addCell: TaskTableViewCell!
    
    var doneButton : UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        doneButton = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped))
        
        toDoNameField.addButtonOnKeyboard(doneButton)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = UIBezierPath(roundedRect: toDoNameField.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: 10, height: 10)).cgPath
        toDoNameField.layer.mask = maskLayer
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func doneButtonTapped() {
        self.view.endEditing(true)
        
        if addCell.taskTextField.text != ""{
            tasks.append(addCell.task!)
            tableView.reloadData()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension TaskViewController : UITableViewDataSource, UITableViewDelegate {
	
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell") as! TaskTableViewCell
        if tasks.count == 0 || indexPath.row == tasks.count {
            addCell = cell
            cell.setupFillTask()
        } else {
            cell.setupWithTask(tasks[indexPath.row])
        }
        
        cell.taskTextField.addButtonOnKeyboard(UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonTapped)))
		
		return cell
	}
	
	func numberOfSections(in tableView: UITableView) -> Int {
		return 1
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tasks.count + 1
	}
	
	
}
