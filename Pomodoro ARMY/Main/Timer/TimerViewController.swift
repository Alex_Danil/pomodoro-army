//
//  TimerViewController.swift
//  Pomodoro ARMY
//
//  Created by Oleksandr Danylenko on 6/6/18.
//  Copyright © 2018 Oleksandr Danylenko. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
	
	@IBOutlet weak var circleTimerView: CTCircularCounter!
	@IBOutlet weak var minuteLabel: UILabel!
	@IBOutlet weak var secondLabel: UILabel!
	@IBOutlet weak var startButton: UIButton!
	@IBOutlet weak var pauseButton: UIButton!
	
	var timerIsOn: Bool = false {
		didSet {
			pauseButton.isHidden = !timerIsOn
		}
	}
	var timer = Timer()
	
	var totalTime: Double = 25*60
	var timeRemaining: Double = 25*60
	
	override func viewDidLoad() {
		super.viewDidLoad()
		pauseButton.isHidden = !timerIsOn

		// Do any additional setup after loading the view.
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		let bgImageView = UIImageView(image: #imageLiteral(resourceName: "gradientBg"))
		bgImageView.frame = self.view.frame
		
		
		let bgColor = UIColor(patternImage: #imageLiteral(resourceName: "gradientBg"))
		
//		self.view.backgroundColor = .black
		self.view.layer.insertSublayer(bgImageView.layer, at: 0)
		circleTimerView.setupCircle()
		
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	@IBAction func startTimer(_ sender: Any) {
		
		if timerIsOn {
			//1
			timer.invalidate()
			//2
			timerIsOn = false
			
			startButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)
		} else {
			startButton.setImage(#imageLiteral(resourceName: "pause"), for: .normal)
			let completionPercentage = CGFloat(((Float(totalTime) - Float(timeRemaining))/Float(totalTime)) )

			circleTimerView.animateCircle(duration: timeRemaining, start:  0, end: CGFloat(1 - completionPercentage))
			//1
			//2
			if !timerIsOn {
				timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerRunning), userInfo: nil, repeats: true)
				//3
				timerIsOn = true
			}
		}
		
		
	}
	
	@objc func timerRunning() {
		timeRemaining -= 1
		
		let minutesLeft = Int(timeRemaining) / 60 % 60
		let secondsLeft = Int(timeRemaining) % 60
		if secondsLeft < 10 {
				secondLabel.text = "0\(secondsLeft)"
		} else {
				secondLabel.text = "\(secondsLeft)"
		}
		minuteLabel.text = "\(minutesLeft)"

//		manageTimerEnd(seconds: timeRemaining)
//		isOnBreak = true
	}
	
	@IBAction func pauseBtnTapped(_ sender: Any) {
		
	}
	
	
	@IBAction func resetTapped(_ sender: Any) {
		//1
		timer.invalidate()
		//2
		timeRemaining = 1500
		secondLabel.text = "00"
		minuteLabel.text = "25"
		//3
		timerIsOn = false
		circleTimerView.setupCircle()
		startButton.setImage(#imageLiteral(resourceName: "play"), for: .normal)


	}
	
		/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
