//
//  CTCircularCounter.swift
//  CarTracker
//
//  Created by Oleksandr Danylenko on 1/11/17.
//  Copyright © 2017 Oleksandr Danylenko. All rights reserved.
//

import UIKit

class CTCircularCounter: UIView {
	
	
	// Only override draw() if you perform custom drawing.
	// An empty implementation adversely affects performance during animation.
	
	var circleLayer: CAShapeLayer!
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.backgroundColor = .clear
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	func setupCircle() {
		// Use UIBezierPath as an easy way to create the CGPath for the layer.
		// The path should be the entire circle.
		
		let endAngle: CGFloat = 4.72 // CGFloat((M_PI * 3) / 2) - 0.1
		let borderFrame = CGRect(x: -2, y: -2, width: (frame.size.width + 4 ), height: (frame.size.width + 4 ))
		
		func degreesToRadians (value:CGFloat) -> CGFloat {
			return value * .pi / 180.0
		}
		
		let circlePath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: (frame.size.width )/2, startAngle: degreesToRadians(value: 90), endAngle: degreesToRadians(value: 450) , clockwise: true)
		
		//         Setup the CAShapeLayer with the path, colors, and line width
		if let sublayers = layer.sublayers, circleLayer != nil {
			for layer in sublayers {
				if layer == circleLayer {
					circleLayer.removeFromSuperlayer()
				}
			}
		}
		
		let bgLayer = CAShapeLayer()
		bgLayer.path = circlePath.cgPath
		bgLayer.fillColor = UIColor.clear.cgColor
		bgLayer.strokeColor = UIColor.white.withAlphaComponent(0.35).cgColor
		bgLayer.lineWidth = 15
		
		layer.addSublayer(bgLayer)
		
		circleLayer = CAShapeLayer()
		circleLayer.path = circlePath.cgPath
		circleLayer.fillColor = UIColor.clear.cgColor
		circleLayer.strokeColor = UIColor.white.withAlphaComponent(1).cgColor
		circleLayer.lineWidth = 15
		
		// Don't draw the circle initially
		circleLayer.strokeEnd = 0
		
		
		// Add the circleLayer to the view's layer's sublayers
		layer.addSublayer(circleLayer)
	}
	
	func animateCircle(duration: TimeInterval , start: CGFloat , end: CGFloat) {
		// We want to animate the strokeEnd property of the circleLayer
		let animation = CABasicAnimation(keyPath: "strokeEnd")
		
		// Set the animation duration appropriately
		animation.duration = duration
		
		// Animate from 0 (no circle) to 1 (full circle)
		animation.fromValue = start
		animation.toValue = end
		
		// Do a linear animation (i.e. the speed of the animation stays the same)
		animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
		
		// Set the circleLayer's strokeEnd property to 1.0 now so that it's the
		// right value when the animation ends.
		circleLayer?.strokeEnd = end
		
		// Do the actual animation
		circleLayer?.add(animation, forKey: "animateCircle")
	}
	
	
}
